import mongoose from "mongoose";
import envVar from "../constant.js";

let connectToMongoDb = () => {
  mongoose.connect(envVar.dbUrl);
  console.log("app is connected to database successfully.");
};

export default connectToMongoDb;

/* 

*/
