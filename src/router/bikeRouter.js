import { Router } from "express";

let bikeRouter = Router();

bikeRouter
  .route("/") //localhost:8000/bike
  .post((req, res, next) => {
    console.log(req.body);
    res.json("bike post");
  })
  .get((req, res, next) => {
    console.log(req.query);
    res.json("bike get");
  })
  .patch((req, res, next) => {
    res.json("bike patch");
  })
  .delete((req, res, next) => {
    res.json("bike delete");
  });

bikeRouter
  .route("/a/:id1/name/:id2") //localhost:8000/bike/a/ram/name/123
  .post((req, res, next) => {
    console.log(req.params); //{id1:"ram",id2:123}
    // req.params give dynamic params in object

    res.json("hello");
  });

//a  => static params
//:id1  => dynamic params
//name => static params
//:id2  => dyamic params

export default bikeRouter;
