import { Router } from "express";
import {
  createUser,
  deleteUser,
  loginUser,
  readAllUser,
  readSpecificUser,
  updateUser,
} from "../controller/userController.js";

let userRouter = Router();

userRouter
  .route("/") //localhost:8000/user
  .post(createUser)
  .get(readAllUser);

userRouter.route("/login").post(loginUser);

userRouter
  .route("/:id") //localhost:8000/user/any
  .get(readSpecificUser)
  .patch(updateUser)
  .delete(deleteUser);

export default userRouter;

/* 
name => string=> required
price => number => required
quantity =>number => required
description => string => required


create user
get all user

get specific user


User.create(req.body)
User.find({})
User.findById(req.params.id)
User.findByIdAndUpdate(req.params.id,req.body)
User.findByIdAndDelete(req.params.id)

*/
