import { Router } from "express";
import {
  createCollege,
  deleteCollege,
  readAllCollege,
  readSpecificCollege,
  updateCollege,
} from "../controller/collegeController.js";
import upload from "../utils/upload.js";

/* import {
  createCollege,
  deleteCollege,
  readAllCollege,
  readSpecificCollege,
  updateCollege,
} from "../controller/collegeController.js"; */

let collegeRouter = Router();

collegeRouter
  .route("/") //localhost:8000/college
  .post(upload.single("collegeImage"), createCollege)
  .get(readAllCollege);

collegeRouter
  .route("/:id") //localhost:8000/college/any
  .get(readSpecificCollege)
  .patch(upload.single("collegeImage"), updateCollege)
  .delete(deleteCollege);

export default collegeRouter;

/* 
name => string=> required
price => number => required
quantity =>number => required
description => string => required


create college
get all college

get specific college


College.create(req.body)
College.find({})
College.findById(req.params.id)
College.findByIdAndUpdate(req.params.id,req.body)
College.findByIdAndDelete(req.params.id)

*/
