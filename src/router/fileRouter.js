import { Router } from "express";
import {
  handleMultipleFile,
  handleSingleFile,
} from "../controller/fileController.js";
import upload from "../utils/upload.js";

let fileRouter = Router();

fileRouter
  .route("/single") //localhost:8000/file/single
  .post(upload.single("docs"), handleSingleFile);
fileRouter.route("/multiple").post(upload.array("docs"), handleMultipleFile);

export default fileRouter;

/*
upload.single("docs") 
used to take form data
it takes file from postman and add to the public
it add file information to  req.file
it add other information to req.body


upload.array("docks")
use to handle multiple file
all are same  but it gives files information in req.files


*/
