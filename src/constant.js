import { config } from "dotenv";
config();

const envVar = {
  secretKey: process.env.SECRET_KEY,
  user: process.env.USER,
  pass: process.env.PASS,
  dbUrl: process.env.DB_URL,
  serverUrl: process.env.SERVER_URL,
};

export default envVar;
