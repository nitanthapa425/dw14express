import envVar from "../constant.js";

export const handleSingleFile = (req, res, next) => {
  // file info comes in req.file
  // other info comes in req.body
  //   console.log(req.body);
  console.log(req.file);
  let link = `${envVar.serverUrl}/${req.file.filename}`;
  res.json({
    success: true,
    message: "file uploaded successfully.",
    result: link,
  });
};

export const handleMultipleFile = (req, res, next) => {
  console.log(req.files);

  let links = req.files.map((item, i) => {
    return `${envVar.serverUrl}/${item.filename}`;
  });
  //[link1,link2,link3]
  res.json({
    success: true,
    message: "file uploaded successfully",
    result: links,
  });
};

/* 

[
  {
    fieldname: 'docs',
    originalname: 'watch2.webp',
    encoding: '7bit',
    mimetype: 'image/webp',
    destination: './public',
    filename: '1714445932863watch2.webp',
    path: 'public\\1714445932863watch2.webp',
    size: 109496
  },
  {
    fieldname: 'docs',
    originalname: 'watch1.webp',
    encoding: '7bit',
    mimetype: 'image/webp',
    destination: './public',
    filename: '1714445932864watch1.webp',
    path: 'public\\1714445932864watch1.webp',
    size: 109706
  },
  {
    fieldname: 'docs',
    originalname: 'profile2.webp',
    encoding: '7bit',
    mimetype: 'image/webp',
    destination: './public',
    filename: '1714445932868profile2.webp',
    path: 'public\\1714445932868profile2.webp',
    size: 180844
  }
]

*/
