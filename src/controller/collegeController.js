import envVar from "../constant.js";
import { College } from "../schema/model.js";

export let createCollege = async (req, res, next) => {
  let data = req.body; //{name:"..",location:".."}
  console.log(req.file);
  let link = `${envVar.serverUrl}/${req.file.filename}`;
  data.collegeImage = link;

  //save data to College table

  /* 
{
  name:"..",
  location:"...",
  collageImage:"link"
}

*/
  try {
    let result = await College.create(data);
    res.status(200).json({
      success: true,
      message: "college created successfully.",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let readAllCollege = async (req, res, next) => {
  try {
    let result = await College.find({});
    res.status(200).json({
      success: true,
      message: "college read successfully.",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let readSpecificCollege = async (req, res, next) => {
  try {
    let result = await College.findById(req.params.id);
    res.status(200).json({
      success: true,
      message: "college read successfully.",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let updateCollege = async (req, res, next) => {
  try {
    res.status(200).json({
      success: true,
      message: "College updated successfully.",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let deleteCollege = async (req, res, next) => {
  try {
    let result = await College.findByIdAndDelete(req.params.id);
    res.status(200).json({
      success: true,
      message: "College deleted successfully.",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};
