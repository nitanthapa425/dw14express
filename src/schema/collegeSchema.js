/* 
fullName, 
password,
email,
gender,
address


*/

import { Schema } from "mongoose";

let collegeSchema = Schema({
  name: {
    type: String,
    required: true,
  },
  location: {
    type: String,
    required: true,
  },
  collegeImage: {
    type: String,
    required: true,
  },
});

export default collegeSchema;
