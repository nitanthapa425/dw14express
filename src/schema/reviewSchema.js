import { Schema } from "mongoose";

let reviewSchema = Schema({
  product: {
    // type: String,
    type: Schema.ObjectId,
    ref: "Products",
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  user: {
    type: Schema.ObjectId,
    ref: "Users",
    required: true,
  },
});

export default reviewSchema;

/* 
name => string=> required
price => number => required
quantity =>number => required
description => string => required
*/

/* 
define structure of review
Schema
model
 */
