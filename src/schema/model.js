import { model } from "mongoose";
import productSchema from "./productSchema.js";
import userSchema from "./userSchema.js";
import teacherSchema from "./teacherSchema.js";
import reviewSchema from "./reviewSchema.js";
import collegeSchema from "./collegeSchema.js";

export let Products = model("Products", productSchema);
export let Users = model("Users", userSchema);
export let Teachers = model("Teachers", teacherSchema);
export let College = model("College", collegeSchema);
export let Reviews = model("Reviews", reviewSchema);

//firstLetterCapital
// variable name and model name must be same
