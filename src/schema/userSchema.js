/* 
fullName, 
password,
email,
gender,
address

find select
sort
limit
skip

localhost:8000/user => give all user
localhost:8000/user?fullName=nitan   => give user whose fullName is nitan
localhost:8000/user?fullName=nitan&gender=male   => give user whose fullName is nitan and gender is male
*/

import { Schema } from "mongoose";

// name must be
// only alphabet

let userSchema = Schema({
  name: {
    type: String,
    required: [true, "name filed is required"],
    lowercase: true,
    trim: true,
    minLength: [3, "name must be at least 3 characters long"],
    maxLength: [30, "name must be at must 30 characters."],
    validate: (value) => {
      let isValid = /^[a-zA-Z]+$/.test(value);
      if (!isValid) {
        let error = new Error("name must be only alphabet");
        throw error;
      }
    },
  },

  /* 
  max 20 character, must have at least one number, must have at least one upper case , must have at least one at least , must have at least one symbol
  
  */
  password: {
    type: String,
    required: true,
    // validate: (value) => {
    //   let isValid = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*\W).{8,20}$/.test(
    //     value
    //   );
    //   if (!isValid) {
    //     let error = new Error(
    //       " password must have min 8 character, max 20 character, must have at least one number, must have at least one upper case , must have at least one lower case , must have at least one symbol"
    //     );
    //     throw error;
    //   }
    // },
  },
  phoneNumber: {
    type: Number,
    required: true,
    lowercase: true,
    trim: true,
    validate: (value) => {
      let strPhoneNumber = String(value);
      let strPhoneNumberLength = strPhoneNumber.length;
      if (strPhoneNumberLength !== 10) {
        let error = new Error("Phone number must be exact 10 digits");
        throw error;
      }
    },
  },
  roll: {
    type: Number,
    required: true,
    trim: true,
    min: [20, "roll must be greater than 20"],
    max: [30, "roll must be less than 30"],
  },
  isMarried: {
    type: Boolean,
    required: true,
    trim: true,
  },
  spouseName: {
    type: String,
    required: true,
    trim: true,
  },
  email: {
    type: String,
    required: true,
    trim: true,
    unique: true,
  },
  gender: {
    type: String,
    required: true,
    trim: true,

    validate: (value) => {
      if (value === "male" || value === "female" || value === "other") {
      } else {
        let error = new Error("gender must be either male or female or other");
        throw error;
      }
    },
  },
  dob: {
    type: Date,
    required: true,
    trim: true,
  },
  location: {
    country: {
      type: String,
    },
    exactLocation: {
      type: String,
    },
  },
  favTeacher: [
    {
      type: String,
    },
  ],

  facSubject: [
    {
      bookName: {
        type: String,
      },
      bookAuthor: {
        type: String,
      },
    },
  ],
});

export default userSchema;

/* 
name => string=> required
price => number => required
quantity =>number => required
description => string => required
*/

/* 
define structure of product
Schema
model


productId
description
userId




inbuilt
custom



 */
