import express, { json } from "express";
import firstRouter from "./src/router/firstRouter.js";
import bikeRouter from "./src/router/bikeRouter.js";
import traineeRouter from "./src/router/traineeRouter.js";
import productRouter from "./src/router/productRouter.js";
import connectToMongoDb from "./src/connecttodb/connectTomongodb.js";
import teacherRouter from "./src/router/teacherRouter.js";
import reviewRouter from "./src/router/reviewRouter.js";
import userRouter from "./src/router/userRouter.js";
import fileRouter from "./src/router/fileRouter.js";
import cors from "cors";
import collegeRouter from "./src/router/collegeRouter.js";

/*
localhost:8000/car1.jpeg
localhost:8000/file/profile.webp

*/
//making express application
let expressApp = express();
expressApp.use(cors());
expressApp.use(json());

expressApp.use(express.static("./public"));
expressApp.use("/name", firstRouter);
expressApp.use("/bike", bikeRouter);
expressApp.use("/trainee", traineeRouter);

expressApp.use("/product", productRouter);
expressApp.use("/teacher", teacherRouter);
expressApp.use("/review", reviewRouter);
expressApp.use("/user", userRouter);
expressApp.use("/file", fileRouter);
expressApp.use("/college", collegeRouter);

connectToMongoDb();
//attach port
expressApp.listen(8000, () => {
  console.log("application is listening at port 8000");
});

/* 

mongodb database
application => database connect


*/
